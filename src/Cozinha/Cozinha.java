package Cozinha;

import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;

public class Cozinha implements GLEventListener,KeyListener{
	
	GLU glu = new GLU();
	GLUT glut = new GLUT();
	private double PortaCimaAbrindo = 0;
	private double PortaBaixoAbrindo = 0;
	private double PortaFogaoAbrindo = 0;
	
	private boolean PortaCimaArmarioAbrir = false;
	private boolean PortaBaixoArmarioAbrir = false;
	private boolean PortaCimaArmarioFechar = false;
	private boolean PortaBaixoArmarioFechar = false;
	private boolean BotaoON = false;
	private boolean BotaoOFF = true;
        private boolean PortaFogaoAbrir = false;
        private boolean PortaFogaoFechar = false;

	public static void main(String[] args) {
		new Cozinha();
                

	}
	
	public Cozinha() {
		
		GLJPanel canvas = new GLJPanel();
        canvas.addGLEventListener(this);
        
        JFrame frame = new JFrame("Cozinha");
        frame.setSize(500, 500);
        frame.getContentPane().add(canvas);
        frame.setVisible(true);
       frame.addKeyListener(this);
        
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Thread(new Runnable() {
                    public void run() {
                        System.exit(0);
                    }
                }).start();
            }
        });
	}

	
	@Override
	public void init(GLAutoDrawable glAuto) {
		Animator a = new Animator(glAuto);
        a.start();
        GL gl = glAuto.getGL();
        gl.glClearColor(1f, 1f, 1f, 1f);
        gl.glEnable(GL.GL_DEPTH_TEST);
        
	}
	
	 public void desenhaGeladeira(GL2 gl){
	        
	        //gl.glRotated(_rotateCacamba, 0, 1, 0);
	       
	        if(PortaCimaArmarioAbrir){
	        	PortaCimaAbrindo  += 0.5;
	        }else if(PortaCimaArmarioFechar){
	            PortaCimaAbrindo -= 0.5;
	        }
	        
	        if(PortaBaixoArmarioAbrir){
	        	PortaBaixoAbrindo  += 0.5;
	        }else if(PortaBaixoArmarioFechar){
	            PortaBaixoAbrindo -= 0.5;
	        }
	        
	        if(BotaoOFF) {
		        if(PortaFogaoAbrir){
		        	PortaFogaoAbrindo  += 0.5;
		        }else if(PortaFogaoFechar){
		            PortaFogaoAbrindo -= 0.5;
		        }
	        }
	        
	        // Armario
	        gl.glPushMatrix();
	            
	            gl.glColor3d(0, 0, 0);	            	           	                	           	            	            	           

	            gl.glPushMatrix();
	            	gl.glColor3d(0.59, 0.29, 0);
	                gl.glScaled(2, 5, 2);
	                glut.glutSolidCube(1);
	            gl.glPopMatrix();
	            
	           	
	            
	            gl.glPushMatrix();
	            
		            gl.glPushMatrix();
		            gl.glColor3d(0.24, 0.17, 0.12);
		            	gl.glTranslated(0, -1.25, 1);
		                gl.glScaled(1.5, 2, 0.2);
		                glut.glutSolidCube(1);
		            gl.glPopMatrix();
	            
	                gl.glColor3d(0.43, 0.21, 0.1);
	                gl.glTranslated(1, -1.25, 1);
	                gl.glRotated(PortaBaixoAbrindo, 0, 1, 0);
	                gl.glTranslated(-1, 0, 0);
	                gl.glScaled(2, 2.5, 0.25);
	                glut.glutSolidCube(1);
	                
	                gl.glColor3d(0.2, 0.2, 0.2);
	                gl.glTranslated(-0.40, 0.35, 0.5);
	                gl.glScaled(0.03, 0.03, 0.5);
	                glut.glutSolidCube(1);
	                
	                gl.glColor3d(0.2, 0.2, 0.2);
	                gl.glTranslated(0, 0, 1.5);
	                gl.glScaled(1, 1, 1);
	                glut.glutSolidSphere(1,20,20);
	                
	            gl.glPopMatrix();
	            
	            gl.glPushMatrix();
	            
		            gl.glPushMatrix();
		            	gl.glColor3d(0.24, 0.17, 0.12);
		            	gl.glTranslated(0, 1.25, 1);
		                gl.glScaled(1.5, 2, 0.2);
		                glut.glutSolidCube(1);
		            gl.glPopMatrix();
	            
	                gl.glColor3d(0.43, 0.21, 0.1);
	                gl.glTranslated(1, 1.25, 1);
	                gl.glRotated(PortaCimaAbrindo, 0, 1, 0);
	                gl.glTranslated(-1, 0, 0);
	                gl.glScaled(2, 2, 0.25);
	                glut.glutSolidCube(1);
	                
	                gl.glColor3d(0.2, 0.2, 0.2);
	                gl.glTranslated(-0.40, -0.35, 0.5);
	                gl.glScaled(0.03, 0.03, 0.5);
	                glut.glutSolidCube(1);
	                
	                gl.glColor3d(0.2, 0.2, 0.2);
	                gl.glTranslated(0, 0, 1.5);
	                gl.glScaled(1, 1, 1);
	                glut.glutSolidSphere(1,20,20);
                
	           gl.glPopMatrix();	            
	            
	        gl.glPopMatrix();
	        
	        //Fogao
	        gl.glPushMatrix();
	        
	        gl.glTranslated(5,-1.25,0);
            
            gl.glColor3d(0, 0, 0);	            	           	                	           	            	            	           

            gl.glPushMatrix();
            	gl.glColor3d(0.44, 0.5, 0.56);
                gl.glScaled(3, 3, 2);
                glut.glutSolidCube(1);
            gl.glPopMatrix();
            
            gl.glPushMatrix();
	        	gl.glColor3d(0, 0, 0);
	        	gl.glTranslated(0, 1.75, 0);
	            gl.glScaled(3, 0.5, 2);
	            glut.glutSolidCube(1);
	        gl.glPopMatrix();
            
            gl.glPushMatrix();
	        	gl.glColor3d(0, 0, 0);
	        	gl.glTranslated(-1.25, 0.90, 2);
	        	gl.glRotated(PortaFogaoAbrindo, 0, 0, 1);
	            gl.glScaled( 0.15, 0.15, 0.15);
	            glut.glutSolidSphere(1,20,20);
	            
	            gl.glColor3d(0.2, 0.2, 0.2);
	            gl.glTranslated(-0.2, 0, 1);
	            gl.glScaled(0.25, 1, 0.25);
	            glut.glutSolidCube(1);
	        gl.glPopMatrix();
	        
	        gl.glPushMatrix();
	        	gl.glColor3d(0, 0, 0);
	        	gl.glTranslated(-0.75, 0.90, 2);
	        	gl.glRotated(PortaFogaoAbrindo, 0, 0, 1);
	            gl.glScaled( 0.15, 0.15, 0.15);
	            glut.glutSolidSphere(1,20,20);
	            
	            gl.glColor3d(0.2, 0.2, 0.2);
	            gl.glTranslated(-0.2, 0, 1);
	            gl.glScaled(0.25, 1, 0.25);
	            glut.glutSolidCube(1);
	        gl.glPopMatrix();
	        
	        gl.glPushMatrix();
	        	gl.glColor3d(0, 0, 0);
	        	gl.glTranslated(-0.25, 0.90, 2);
	        	gl.glRotated(PortaFogaoAbrindo, 0, 0, 1);
	            gl.glScaled( 0.15, 0.15, 0.15);
	            glut.glutSolidSphere(1,20,20);
	            
	            gl.glColor3d(0.2, 0.2, 0.2);
	            gl.glTranslated(-0.2, 0, 1);
	            gl.glScaled(0.25, 1, 0.25);
	            glut.glutSolidCube(1);
	        gl.glPopMatrix();
	        
	        gl.glPushMatrix();
	        	gl.glColor3d(0, 0, 0);
	        	gl.glTranslated(0.25, 0.90, 2);
	        	gl.glRotated(PortaFogaoAbrindo, 0, 0, 1);
	            gl.glScaled( 0.15, 0.15, 0.15);
	            glut.glutSolidSphere(1,20,20);
	            
	            gl.glColor3d(0.2, 0.2, 0.2);
	            gl.glTranslated(-0.2, 0, 1);
	            gl.glScaled(0.25, 1, 0.25);
	            glut.glutSolidCube(1);
	        gl.glPopMatrix();
	        
	        gl.glPushMatrix();
	        	if(BotaoON)
	        		gl.glColor3d(1, 0, 0);
	        	else if (BotaoOFF)
	        		gl.glColor3d(0, 0, 0);
	        	gl.glTranslated(0.75, 0.90, 2);
	            gl.glScaled( 0.15, 0.15, 0.15);
	            glut.glutSolidCube(1);
            gl.glPopMatrix();
        
            
            gl.glPushMatrix();
            
	            gl.glPushMatrix();
		        	gl.glColor3d(0.24, 0.17, 0.12);
		        	gl.glTranslated(0, -0.5, 0.95);
		            gl.glScaled(2.5, 2, 0.25);
		            glut.glutSolidCube(1);
		        gl.glPopMatrix();
            
                gl.glColor3d(0.41, 0.41, 0.41);
                gl.glTranslated(0, -1.5, 1);
                gl.glRotated(PortaFogaoAbrindo, 1, 0, 0);
                gl.glTranslated(0, 1, 0);
                gl.glScaled(2.5, 2, 0.25);
                glut.glutSolidCube(1);
                
                gl.glColor3d(0.2, 0.2, 0.2);
                gl.glTranslated(-0.25, 0.3, 0.5);
                gl.glScaled(0.03, 0.03, 2);
                glut.glutSolidCube(1);
                
                gl.glColor3d(0.2, 0.2, 0.2);
                gl.glTranslated(18, 0, 0);
                gl.glScaled(1, 1, 1);
                glut.glutSolidCube(1);
                
                gl.glColor3d(0.2, 0.2, 0.2);
                gl.glTranslated(-9, 0, 0.5);
                gl.glScaled(25, 2, 0.2);
                glut.glutSolidCube(1);
//                
//                gl.glColor3d(0.2, 0.2, 0.2);
//                gl.glTranslated(0, 0, 1.5);
//                gl.glScaled(1, 1, 1);
//                glut.glutSolidSphere(1,20,20);
                
            gl.glPopMatrix();           
            
        gl.glPopMatrix();
	        
	        
	        if(PortaBaixoAbrindo >= 90)
	            PortaBaixoArmarioAbrir = false;
	        else if(PortaBaixoAbrindo <= 0)
	            PortaBaixoArmarioFechar = false;
	        
	        if(PortaCimaAbrindo >= 90)
	            PortaCimaArmarioAbrir = false;
	        else if(PortaCimaAbrindo <= 0)
	            PortaCimaArmarioFechar = false;
	        
	        if(PortaFogaoAbrindo >= 90)
	            PortaFogaoAbrir = false;
	        else if(PortaFogaoAbrindo <= 0)
	            PortaFogaoFechar = false;
	        
	    }
	 
	 	@Override
	    public void display(GLAutoDrawable glAuto) {

	        GL2 gl = glAuto.getGL().getGL2();
	        gl.glClear(GL.GL_COLOR_BUFFER_BIT |
	                   GL.GL_DEPTH_BUFFER_BIT
	        );
	        
	        gl.glLoadIdentity();
	        gl.glTranslated(0,0,-20);
	        //gl.glRotated(gGraus--, 0, 1, 0);
	        //gl.glTranslated(0,0,-10);
	        desenhaGeladeira(gl);   
	    }
	 
	@Override
	public void keyPressed(KeyEvent e) {
		int event = e.getKeyCode();
		
		 switch (event) {
	        
	    case KeyEvent.VK_G:
			if(PortaBaixoArmarioAbrir){
	            PortaBaixoArmarioFechar = false;
	            PortaBaixoArmarioAbrir = false;
	        }else{
	            PortaBaixoArmarioAbrir = true;
	            PortaBaixoArmarioFechar = false;
	        }break;
	        
	    case KeyEvent.VK_H:
			if(PortaBaixoArmarioFechar){
	            PortaBaixoArmarioFechar = false;
	            PortaBaixoArmarioAbrir = false;
	        }else{
	            PortaBaixoArmarioAbrir = false;
	            PortaBaixoArmarioFechar = true;
	        }break;
	        
	    case KeyEvent.VK_T:
			if(PortaCimaArmarioAbrir){
	            PortaCimaArmarioFechar = false;
	            PortaCimaArmarioAbrir = false;
	        }else{
	            PortaCimaArmarioAbrir = true;
	            PortaCimaArmarioFechar = false;
	        }break;
	        
	    case KeyEvent.VK_Y:
			if(PortaCimaArmarioFechar){
	            PortaCimaArmarioFechar = false;
	            PortaCimaArmarioAbrir = false;
	        }else{
	            PortaCimaArmarioAbrir = false;
	            PortaCimaArmarioFechar = true;
	        }break;
	        
	    case KeyEvent.VK_L:
			if(BotaoON){
	            BotaoON = false;
	            BotaoOFF = true;
	        }else{
	            BotaoON = true;
	            BotaoOFF = false;
	        }break;
	        
	    case KeyEvent.VK_F:
			if(PortaFogaoAbrir){
	            PortaFogaoFechar = false;
	            PortaFogaoAbrir = false;
	        }else{
	            PortaFogaoAbrir = true;
	            PortaFogaoFechar = false;
	        }break;
	        
	    case KeyEvent.VK_R:
			if(PortaFogaoFechar){
	            PortaFogaoFechar = false;
	            PortaFogaoAbrir = false;
	        }else{
	            PortaFogaoAbrir = false;
	            PortaFogaoFechar = true;
	        }break;
		 
		 }
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reshape(GLAutoDrawable gLAutoDrawable, int x, int y, int w, int h) {
		  
        GL2 gl = gLAutoDrawable.getGL().getGL2(); 
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(60,1,1,300);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslated(0,0,-10);
    }

}
